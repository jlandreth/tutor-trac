﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AdminMenu : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

       if (Convert.ToInt32(Session["key"].ToString()) == 2)
        {
            Response.Redirect("Default.aspx");
        }
    }
}
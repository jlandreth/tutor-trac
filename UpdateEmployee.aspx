﻿<%@ Page Language="C#" MasterPageFile="~/MasterPageOne.master" AutoEventWireup="true" CodeFile="UpdateEmployee.aspx.cs" Inherits="UpdateEmployee" %>
<%@ MasterType VirtualPath="~/MasterPageOne.master" %>

<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Configuration" %>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server" Visible="true">
    <h1><asp:Label ID="Label1" runat="server" Text="Update Employee" ></asp:Label></h1>
    <p>
        <asp:DropDownList ID="DropDownSearch" runat="server" DataSourceID="SqlDataSource3" DataTextField="Column1" DataValueField="empId">
            <asp:ListItem>Student ID</asp:ListItem>
            <asp:ListItem>Last Name</asp:ListItem>
        </asp:DropDownList>
   <br />
        <asp:Label ID="Label2" runat="server" Text="Search by "></asp:Label>
        <asp:TextBox ID="EmpSearch" runat="server"></asp:TextBox>
        <asp:Button ID="Button1" runat="server" Text="Submit" />
    </p>





           
     
      
    <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:TUTORTRACConnectionString %>" SelectCommand="Select lname+', '+fname, empId
from Employee"></asp:SqlDataSource>





           
     
      
   <!--         VFNAME:
            <asp:TextBox ID="VFNAMETextBox" runat="server" Text='<%# Bind("VFNAME") %>' />
            <br />
            VLNAME:
            <asp:TextBox ID="VLNAMETextBox" runat="server" Text='<%# Bind("VLNAME") %>' />  -->
    <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:TUTORTRACConnectionString %>" DeleteCommand="DELETE FROM [CRLA_Employee] WHERE [EmpID] = @EmpID" InsertCommand="INSERT INTO [CRLA_Employee] ([StuID], [LName], [FName], [Street1], [Street2], [City], [State], [Zip], [Email], [CellNum], [HomeNum], [IsSupervisor], [HireDate], [TermDate], [CycStDate], [CycEndDate], [Active], [Rehire]) VALUES (@StuID, @LName, @FName, @Street1, @Street2, @City, @State, @Zip, @Email, @CellNum, @HomeNum, @IsSupervisor, @HireDate, @TermDate, @CycStDate, @CycEndDate, @Active, @Rehire)" SelectCommand="SELECT [EmpID], [StuID], [LName], [FName], [Street1], [Street2], [City], [State], [Zip], [Email], [CellNum], [HomeNum], [IsSupervisor], [HireDate], [TermDate], [CycStDate], [CycEndDate], [Active], [Rehire] FROM [Employee]" UpdateCommand="UPDATE [CRLA_Employee] SET [StuID] = @StuID, [LName] = @LName, [FName] = @FName, [Street1] = @Street1, [Street2] = @Street2, [City] = @City, [State] = @State, [Zip] = @Zip, [Email] = @Email, [CellNum] = @CellNum, [HomeNum] = @HomeNum, [IsSupervisor] = @IsSupervisor, [HireDate] = @HireDate, [TermDate] = @TermDate, [CycStDate] = @CycStDate, [CycEndDate] = @CycEndDate, [Active] = @Active, [Rehire] = @Rehire WHERE [EmpID] = @EmpID">
        <DeleteParameters>
            <asp:Parameter Name="EmpID" Type="Int32" />
        </DeleteParameters>
        <FilterParameters>
            <asp:ControlParameter ControlID="DropDownSearch" Name="newparameter" PropertyName="SelectedValue" />
        </FilterParameters>
        <InsertParameters>
            <asp:Parameter Name="StuID" Type="String" />
            <asp:Parameter Name="LName" Type="String" />
            <asp:Parameter Name="FName" Type="String" />
            <asp:Parameter Name="Street1" Type="String" />
            <asp:Parameter Name="Street2" Type="String" />
            <asp:Parameter Name="City" Type="String" />
            <asp:Parameter Name="State" Type="String" />
            <asp:Parameter Name="Zip" Type="String" />
            <asp:Parameter Name="Email" Type="String" />
            <asp:Parameter Name="CellNum" Type="String" />
            <asp:Parameter Name="HomeNum" Type="String" />
            <asp:Parameter Name="IsSupervisor" Type="String" />
            <asp:Parameter DbType="Date" Name="HireDate" />
            <asp:Parameter DbType="Date" Name="TermDate" />
            <asp:Parameter DbType="Date" Name="CycStDate" />
            <asp:Parameter DbType="Date" Name="CycEndDate" />
            <asp:Parameter Name="Active" Type="String" />
            <asp:Parameter Name="Rehire" Type="String" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="StuID" Type="String" />
            <asp:Parameter Name="LName" Type="String" />
            <asp:Parameter Name="FName" Type="String" />
            <asp:Parameter Name="Street1" Type="String" />
            <asp:Parameter Name="Street2" Type="String" />
            <asp:Parameter Name="City" Type="String" />
            <asp:Parameter Name="State" Type="String" />
            <asp:Parameter Name="Zip" Type="String" />
            <asp:Parameter Name="Email" Type="String" />
            <asp:Parameter Name="CellNum" Type="String" />
            <asp:Parameter Name="HomeNum" Type="String" />
            <asp:Parameter Name="IsSupervisor" Type="String" />
            <asp:Parameter DbType="Date" Name="HireDate" />
            <asp:Parameter DbType="Date" Name="TermDate" />
            <asp:Parameter DbType="Date" Name="CycStDate" />
            <asp:Parameter DbType="Date" Name="CycEndDate" />
            <asp:Parameter Name="Active" Type="String" />
            <asp:Parameter Name="Rehire" Type="String" />
            <asp:Parameter Name="EmpID" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <asp:DetailsView ID="DetailsView1" runat="server" AutoGenerateRows="False" DataSourceID="SqlDataSource2" Height="50px" Width="305px">
        <Fields>
            <asp:CommandField ButtonType="Button" EditText="Update Employee" ShowEditButton="True" />
            <asp:BoundField DataField="EmpID" HeaderText="EmpID" InsertVisible="False" ReadOnly="True" SortExpression="EmpID" Visible="False" />
            <asp:BoundField DataField="StuID" HeaderText="Student ID" SortExpression="StuID" />
            <asp:BoundField DataField="LName" HeaderText="Last Name" SortExpression="LName" />
            <asp:BoundField DataField="FName" HeaderText="First Name" SortExpression="FName" />
            <asp:BoundField DataField="Street1" HeaderText="Street1" SortExpression="Street1" />
            <asp:BoundField DataField="Street2" HeaderText="Street2" SortExpression="Street2" />
            <asp:BoundField DataField="City" HeaderText="City" SortExpression="City" />
            <asp:BoundField DataField="State" HeaderText="State" SortExpression="State" />
            <asp:BoundField DataField="Zip" HeaderText="Zip" SortExpression="Zip" />
            <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
            <asp:BoundField DataField="CellNum" HeaderText="CellNum" SortExpression="CellNum" />
            <asp:BoundField DataField="HomeNum" HeaderText="HomeNum" SortExpression="HomeNum" />
            <asp:BoundField DataField="IsSupervisor" HeaderText="IsSupervisor" SortExpression="IsSupervisor" />
            <asp:BoundField DataField="HireDate" HeaderText="HireDate" SortExpression="HireDate" />
            <asp:BoundField DataField="TermDate" HeaderText="TermDate" SortExpression="TermDate" />
            <asp:BoundField DataField="CycStDate" HeaderText="CycStDate" SortExpression="CycStDate" />
            <asp:BoundField DataField="CycEndDate" HeaderText="CycEndDate" SortExpression="CycEndDate" />
            <asp:BoundField DataField="Active" HeaderText="Active" SortExpression="Active" />
            <asp:BoundField DataField="Rehire" HeaderText="Rehire" SortExpression="Rehire" />
        </Fields>
        <FooterStyle VerticalAlign="Top" />
</asp:DetailsView>
    <br />
         <!--   <asp:TextBox ID="TextBox1" runat="server" Visible="False"></asp:TextBox> -->
            &nbsp;&nbsp;&nbsp;
    <br />
           
    Add Additional Info on Employee<br />
    <asp:Button ID="Button2" runat="server" Text="Training" PostBackUrl="~/NewTrainingInfo.aspx" />
    &nbsp;
    <asp:Button ID="Button3" runat="server" Text="Hours" />
&nbsp;
    <asp:Button ID="Button4" runat="server" Text="Certifications" />
    <br />
   
    <br />
   
    <br />

</asp:Content>

